@extends('layouts.admin.master')
@section('title','Files')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Files list</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand name: activate to sort column descending">File name</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand name: activate to sort column descending">Version</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand name: activate to sort column descending">OS version</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand name: activate to sort column descending">Price</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand name: activate to sort column descending">File size (KB)</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand name: activate to sort column descending">Device</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand name: activate to sort column descending">File type</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Brand name: activate to sort column descending">Instruction</th>

                                            <th  tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Action</th>
                                        </thead>
                                        <tbody>
                                        @foreach($files as $file)
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">{{ $file->file_name }}</td>
                                            <td>{{ $file->version }}</td>
                                            <td>{{ $file->os_version }}</td>
                                            <td>{{ $file->price }}</td>
                                            <td>{{ $file->file_size }}</td>
                                            <td>{{ $file->device->name }}</td>
                                            <td>{{ $file->ftype->type_name ?? 'Problem here' }}</td>{{--, id asche but related type name asche na--}}
                                            <td>{{ $file->instruction->instructions_title }}</td>
                                            <td>

                                                <a href="{{ route('file.edit',$file->id) }}" class="btn btn-primary" data-toggle="tooltip">
                                                    Edit
                                                </a>
                                                <form class="d-inline-block" action="{{ route('file.destroy',$file->id) }}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button class="btn btn-danger">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                         </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                                <ul class="pagination">
                                    paginate part
                                </ul>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
