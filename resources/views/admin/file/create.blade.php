@extends('layouts.admin.master')
@section('title','Add File')
@section('content')
    <section class="content">
        <div class="row">

            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add File</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{ route('file.store') }}" method="post">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="brandName">File Name</label>
                                <input type="text" class="form-control" id="fileName" placeholder="File Name" name="file_name">
                            </div>

                            <div class="form-group">
                                <label for="version">Version</label>
                                <input type="text" class="form-control" id="version" placeholder="Version" name="version">
                            </div>

                            <div class="form-group">
                                <label for="osversion">OS Version</label>
                                <input type="text" class="form-control" id="osversion" placeholder="OS Version" name="os_version">
                            </div>

                            <div class="form-group">
                                <label for="price">Price</label>
                                <input type="text" class="form-control" id="price" placeholder="Price" name="price" value="Free">
                            </div>

                            <div class="form-group">
                                <label for="fileSiza">File size</label>
                                <input type="text" class="form-control" id="fileSiza" placeholder="File size" name="file_size">
                            </div>

                            <div class="form-group">
                                <label>Device</label>
                                <select class="form-control" name="device_id">
                                    <option value="">Select Device</option>
                                    @foreach($devices as $device)
                                        <option value="{{ $device->id }}">{{ $device->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>File type</label>
                                <select class="form-control" name="type_id">
                                    <option value="">Select type</option>
                                    @foreach($types as $type)
                                        <option value="{{ $type->id }}">{{ $type->type_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Instruction</label>
                                <select class="form-control" name="instruction_id">
                                    <option value="">Select instruction</option>
                                    @foreach($instructions as $instruction)
                                        <option value="{{ $instruction->id }}">{{ $instruction->instructions_title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>

        </div>
        <!-- /.row -->
    </section>
@endsection
