@extends('layouts.admin.master')
@section('title','Add Barnd')
@section('content')
    <section class="content">
        <div class="row">

            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add New Barnd</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{ route('brand.store') }}" method="post">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="brandName">Brand Name</label>
                                <input type="text" class="form-control" id="brandName" placeholder="Brand Name" name="brand_name">
                            </div>
                            <div class="form-group">
                                <label for="brandDetails">Details</label>
                                <input type="text" class="form-control" id="brandDetails" placeholder="Brand Details" name="brand_details">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>

        </div>
        <!-- /.row -->
    </section>
@endsection
