@extends('layouts.admin.master')
@section('title','Edit Brand')
@section('content')
    <section class="content">
        <div class="row">

            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Barnd</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{ route('brand.update',$brand->id) }}" method="post">
                        @csrf
                        @method('put')
                        <div class="box-body">
                            <div class="form-group">
                                <label for="brandName">Brand Name</label>
                                <input type="text" class="form-control" id="brandName" placeholder="Brand Name" name="brand_name" value="{{ $brand->brand_name }}">
                            </div>
                            <div class="form-group">
                                <label for="brandDetails">Details</label>
                                <input type="text" class="form-control" id="brandDetails" placeholder="Brand Details" name="brand_details" value="{{ $brand->brand_details }}">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>

        </div>
        <!-- /.row -->
    </section>
@endsection
