@extends('layouts.admin.master')
@section('title','Edit Instruction')
@section('content')
    <section class="content">
        <div class="row">

            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit instruction</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{ route('instruction.update',$instruction->id) }}" method="post">
                        @csrf
                        @method('put')
                        <div class="box-body">
                            <div class="form-group">
                                <label for="instruction">Instruction Title</label>
                                <input type="text" class="form-control" id="instruction" placeholder="Instruction Title" name="instructions_title" value="{{ $instruction->instructions_title }}">
                            </div>
                            <div class="form-group">
                                <label for="details">Details</label>
                                <input type="text" class="form-control" id="details" placeholder="Details" name="details" value="{{ $instruction->details }}">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>

        </div>
        <!-- /.row -->
    </section>
@endsection
