@extends('layouts.admin.master')
@section('title','Add new instruction')
@section('content')
    <section class="content">
        <div class="row">

            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add new instruction</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{ route('instruction.store') }}" method="post">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="instruction">Instruction Title</label>
                                <input type="text" class="form-control" id="instruction" placeholder="Instruction Title" name="instructions_title">
                            </div>
                            <div class="form-group">
                                <label for="details">Details</label>
                                <input type="text" class="form-control" id="details" placeholder="Details" name="details">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>

        </div>
        <!-- /.row -->
    </section>
@endsection
