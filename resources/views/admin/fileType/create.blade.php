@extends('layouts.admin.master')
@section('title','Add file type')
@section('content')
    <section class="content">
        <div class="row">

            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add File type</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{ route('fileType.store') }}" method="post">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="typeName">Type Name</label>
                                <input type="text" class="form-control" id="typeName" placeholder="File type Name" name="type_name">
                            </div>
                            <div class="form-group">
                                <label for="typeDetails">Details</label>
                                <input type="text" class="form-control" id="typeDetails" placeholder="Type Details" name="type_details">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>

        </div>
        <!-- /.row -->
    </section>
@endsection
