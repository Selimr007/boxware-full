<script src="{{ asset('./js/jquery.min.js.download') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('./js/bootstrap.min.js.download') }}"></script>
<!-- FastClick -->
<script src="{{ asset('./js/fastclick.js.download') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('./js/adminlte.min.js.download') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('./js/jquery.sparkline.min.js.download') }}"></script>
<!-- jvectormap  -->
<script src="{{ asset('./js/jquery-jvectormap-1.2.2.min.js.download') }}"></script>
<script src="{{ asset('./js/jquery-jvectormap-world-mill-en.js.download') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('./js/jquery.slimscroll.min.js.download') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('./js/Chart.js.download') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('./js/dashboard2.js.download') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('./js/demo.js.download') }}"></script>
