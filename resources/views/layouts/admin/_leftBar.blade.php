<section class="sidebar" style="height: auto;">

    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu tree" data-widget="tree">

        <li class="treeview">
            <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Brand</span>
                <span class="pull-right-container">
              <span class="label label-primary pull-right">$</span>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('brand.index') }}"><i class="fa fa-circle-o"></i>Brand List</a></li>
                <li><a href="{{ route('brand.create') }}"><i class="fa fa-circle-o"></i>Add Brand</a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-files-o"></i>
                <span>File Type</span>
                <span class="pull-right-container">
              <span class="label label-primary pull-right">$</span>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('fileType.index') }}"><i class="fa fa-circle-o"></i>File Types</a></li>
                <li><a href="{{ route('fileType.create') }}"><i class="fa fa-circle-o"></i>Add File type</a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Instruction</span>
                <span class="pull-right-container">
              <span class="label label-primary pull-right">$</span>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('instruction.index') }}"><i class="fa fa-circle-o"></i>Instructions</a></li>
                <li><a href="{{ route('instruction.create') }}"><i class="fa fa-circle-o"></i>Add instruction</a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Device</span>
                <span class="pull-right-container">
              <span class="label label-primary pull-right">$</span>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('device.index') }}"><i class="fa fa-circle-o"></i>Device list</a></li>
                <li><a href="{{ route('device.create') }}"><i class="fa fa-circle-o"></i>Add device</a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-files-o"></i>
                <span>File</span>
                <span class="pull-right-container">
              <span class="label label-primary pull-right">$</span>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('file.index') }}"><i class="fa fa-circle-o"></i>Files list</a></li>
                <li><a href="{{ route('file.create') }}"><i class="fa fa-circle-o"></i>Add File</a></li>
            </ul>
        </li>

        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
    </ul>
</section>
