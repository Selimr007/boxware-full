<?php

namespace App\Http\Controllers\admin;

use App\admin\Device;
use App\admin\File;
use App\admin\FileType;
use App\admin\Instruction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['files'] = File::all();
        return view('admin/file/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['devices'] = Device::all();
        $data['types'] = FileType::all();
        $data['instructions'] = Instruction::all();

        return view('admin/file/create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['file_name'] = $request->file_name;
        $data['version'] = $request->version;
        $data['os_version'] = $request->os_version;
        $data['price'] = $request->price;
        $data['file_size'] = $request->file_size;
        $data['device_id'] = $request->device_id;
        $data['type_id'] = $request->type_id;
        $data['instruction_id'] = $request->instruction_id;

        File::create($data);
        return redirect()->route('file.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['devices'] = Device::all();
        $data['types'] = FileType::all();
        $data['instructions'] = Instruction::all();
        $data['file'] = File::findorfail($id);

        return view('admin/file/edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data['file_name'] = $request->file_name;
        $data['version'] = $request->version;
        $data['os_version'] = $request->os_version;
        $data['price'] = $request->price;
        $data['file_size'] = $request->file_size;
        $data['device_id'] = $request->device_id;
        $data['type_id'] = $request->type_id;
        $data['instruction_id'] = $request->instruction_id;

        File::findOrfail($id)->update($data);
        return redirect()->route('file.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        File::findOrfail($id)->delete();
        return redirect()->route('file.index');
    }
}
