<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class FileType extends Model
{
    protected $fillable = ['type_name','type_details'];
}
