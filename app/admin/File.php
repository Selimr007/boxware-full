<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable=['file_name','version','os_version','price','file_size','device_id','type_id','instruction_id'];

    public function device()
    {
        return $this->belongsTo(Device::class);
    }

    public function instruction()
    {
        return $this->belongsTo(Instruction::class);
    }

    public function ftype()
    {
        return $this->belongsTo(FileType::class);
    }
}
