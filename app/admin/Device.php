<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $fillable = ['name','model','brand_id'];




    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
