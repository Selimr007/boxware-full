<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class Instruction extends Model
{
    protected $fillable = ['instructions_title','details'];
}
