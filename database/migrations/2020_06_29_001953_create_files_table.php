<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->string('file_name');
            $table->string('version');
            $table->string('os_version');
            $table->string('price')->default('Free');
            $table->double('file_size',7,2);
            $table->unsignedBigInteger('device_id');
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('instruction_id');

            $table->foreign('device_id')->references('id')->on('devices');
            $table->foreign('type_id')->references('id')->on('file_types');
            $table->foreign('instruction_id')->references('id')->on('instructions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
